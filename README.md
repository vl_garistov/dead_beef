# 0xDEAD_BEEF

A simple demonstration of dynamic control of 7-segment indicators with a variable refresh rate. Developed for Technical School "Electronic Systems" in Sofia, Bulgaria.

The refresh rate is controlled using a potentiometer. The indicators used are of the common-cathode type and multiplexing is done with bipolar transistors.
