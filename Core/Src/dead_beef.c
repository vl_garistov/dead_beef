/*
 * dead_beef.c
 *
 *  Created on: 18 Feb. 2023
 *      Author: Vladimir Garistov <vl.garistov@gmail.com>
 */

#include "dead_beef.h"
#include "main.h"
#include <inttypes.h>

static const uint32_t segm_lut[16] =
{
	DIGIT_0_SEGMENTS, DIGIT_1_SEGMENTS, DIGIT_2_SEGMENTS, DIGIT_3_SEGMENTS,
	DIGIT_4_SEGMENTS, DIGIT_5_SEGMENTS, DIGIT_6_SEGMENTS, DIGIT_7_SEGMENTS,
	DIGIT_8_SEGMENTS, DIGIT_9_SEGMENTS, DIGIT_A_SEGMENTS, DIGIT_B_SEGMENTS,
	DIGIT_C_SEGMENTS, DIGIT_D_SEGMENTS, DIGIT_E_SEGMENTS, DIGIT_F_SEGMENTS
};
static const uint32_t digit_pins[NUM_OF_DIGITS] = {DIG_0_Pin, DIG_1_Pin, DIG_2_Pin, DIG_3_Pin};
static GPIO_TypeDef * const digit_ports[NUM_OF_DIGITS] = {DIG_0_GPIO_Port, DIG_1_GPIO_Port, DIG_2_GPIO_Port, DIG_3_GPIO_Port};
static const uint16_t text[NUM_OF_WORDS] = {0xDEAD, 0xBEEF};

static volatile uint8_t word_counter = 0;

void change_word(void)
{
	word_counter = (word_counter + 1) % NUM_OF_WORDS;
}

void change_frequency(uint16_t pot_val)
{
	uint32_t new_period = (uint32_t) pot_val * (uint32_t) (MAX_TIM1_PERIOD - MIN_TIM1_PERIOD) / (uint32_t) ADC_MAX_VAL + MIN_TIM1_PERIOD;
	LL_TIM_SetAutoReload(TIM1, new_period);
}

void next_digit(void)
{
	static uint8_t digit = 0;

	LL_GPIO_ResetOutputPin(digit_ports[digit], digit_pins[digit]);
	LL_GPIO_ResetOutputPin(SEGMENTS_GPIO_Port, ALL_SEGMENTS);
	digit = (digit + 1) % NUM_OF_DIGITS;
	uint8_t digit_val = (text[word_counter] >> ((NUM_OF_DIGITS - digit - 1) * 4)) & 0xF;
	LL_GPIO_SetOutputPin(SEGMENTS_GPIO_Port, segm_lut[digit_val]);
	LL_GPIO_SetOutputPin(digit_ports[digit], digit_pins[digit]);
}
