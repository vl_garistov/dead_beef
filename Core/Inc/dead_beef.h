/*
 * deaf_beef.h
 *
 *  Created on: 18 Feb. 2023
 *      Author: Vladimir Garistov <vl.garistov@gmail.com>
 */

#ifndef INC_DEAD_BEEF_H_
#define INC_DEAD_BEEF_H_

#include <inttypes.h>

#define WORD_CHANGE_PERIOD	2000	// ms
#define ADC_CONV_PERIOD		100		// ms

#define MAX_TIM1_PERIOD		50000	// 4 Hz
#define MIN_TIM1_PERIOD		500		// 400 Hz
#define ADC_MAX_VAL			4095

#define NUM_OF_WORDS		2
#define NUM_OF_DIGITS		4

#define DIGIT_0_SEGMENTS	(SEGM_A_Pin | SEGM_B_Pin | SEGM_C_Pin | SEGM_D_Pin | SEGM_E_Pin | SEGM_F_Pin)
#define DIGIT_1_SEGMENTS	(SEGM_B_Pin | SEGM_C_Pin)
#define DIGIT_2_SEGMENTS	(SEGM_A_Pin | SEGM_B_Pin | SEGM_D_Pin | SEGM_E_Pin | SEGM_G_Pin)
#define DIGIT_3_SEGMENTS	(SEGM_A_Pin | SEGM_B_Pin | SEGM_C_Pin | SEGM_D_Pin | SEGM_G_Pin)
#define DIGIT_4_SEGMENTS	(SEGM_B_Pin | SEGM_C_Pin | SEGM_F_Pin | SEGM_G_Pin)
#define DIGIT_5_SEGMENTS	(SEGM_A_Pin | SEGM_C_Pin | SEGM_D_Pin | SEGM_F_Pin | SEGM_G_Pin)
#define DIGIT_6_SEGMENTS	(SEGM_A_Pin | SEGM_C_Pin | SEGM_D_Pin | SEGM_E_Pin | SEGM_F_Pin | SEGM_G_Pin)
#define DIGIT_7_SEGMENTS	(SEGM_A_Pin | SEGM_B_Pin | SEGM_C_Pin)
#define DIGIT_8_SEGMENTS	(SEGM_A_Pin | SEGM_B_Pin | SEGM_C_Pin | SEGM_D_Pin | SEGM_E_Pin | SEGM_F_Pin | SEGM_G_Pin)
#define DIGIT_9_SEGMENTS	(SEGM_A_Pin | SEGM_B_Pin | SEGM_C_Pin | SEGM_D_Pin | SEGM_F_Pin | SEGM_G_Pin)
#define DIGIT_A_SEGMENTS	(SEGM_A_Pin | SEGM_B_Pin | SEGM_C_Pin | SEGM_E_Pin | SEGM_F_Pin | SEGM_G_Pin)
#define DIGIT_B_SEGMENTS	(SEGM_C_Pin | SEGM_D_Pin | SEGM_E_Pin | SEGM_F_Pin | SEGM_G_Pin)
#define DIGIT_C_SEGMENTS	(SEGM_A_Pin | SEGM_D_Pin | SEGM_E_Pin | SEGM_F_Pin)
#define DIGIT_D_SEGMENTS	(SEGM_B_Pin | SEGM_C_Pin | SEGM_D_Pin | SEGM_E_Pin | SEGM_G_Pin)
#define DIGIT_E_SEGMENTS	(SEGM_A_Pin | SEGM_D_Pin | SEGM_E_Pin | SEGM_F_Pin | SEGM_G_Pin)
#define DIGIT_F_SEGMENTS	(SEGM_A_Pin | SEGM_E_Pin | SEGM_F_Pin | SEGM_G_Pin)
#define ALL_SEGMENTS		(SEGM_A_Pin | SEGM_B_Pin | SEGM_C_Pin | SEGM_D_Pin | SEGM_E_Pin | SEGM_F_Pin | SEGM_G_Pin | SEGM_DP_Pin)

void change_word(void);
void change_frequency(uint16_t pot_val);
void next_digit(void);

#endif /* INC_DEAD_BEEF_H_ */
